var app = angular.module('mapFunBox', []);

app.service("map", function($rootScope,$q){
    var that = this;

    this.mapReady = function(){
        var deffered = $q.defer();
        ymaps.ready(function(){
            deffered.resolve(true);
            return deffered.promise;
        });
        return deffered.promise;
    };
    this.init = function(){
        this.map = new ymaps.Map("map", {
            center: [55.76, 37.64],
            zoom: 10
        });
    };
    this.geocode = function(point){//direct\indirect
        var map = this.map,
            deffered = $q.defer();
        ymaps.geocode(point).then(function(res){
            deffered.resolve(res);
        });
        return deffered.promise;
    };
    this.setCenter = function(coord){
        var map = this.map;
        map.setCenter(coord);
    };
    this.route = function(arrPoints, names) {
        var map = this.map;
        map.geoObjects.removeAll();
        ymaps.route(arrPoints, {"test": "testProp"}).then(function (route) {
            route.getWayPoints().options.set({
                draggable: true
            });
            map.geoObjects.add(route);

            route.getWayPoints().each(function (wayPoint, index) {
                //if it coord
                if(Object.prototype.toString.call(arrPoints[index]) == "[object Array]"){
                    that.geocode(wayPoint.geometry.getBounds()[0]).then(function(res){
                        wayPoint.properties.set("balloonContent", res.geoObjects.get(0).properties.get("name"))
                        names[index] = res.geoObjects.get(0).properties.get("name");
                    });
                }
                wayPoint.events.add("dragend", function (event) {
                    arrPoints[index] = wayPoint.geometry.getBounds()[0];
                    $rootScope.$apply();
                })
            });
            route.getWayPoints().get(0).properties.set("balloonContent", "testContent") //work
        });
    };
});
app.directive("sortable", function(){
    return{
        restrict: "A",
        scope: {
            change: "&"
        },
        link: function(scope, element, attrs){
            var indexBefore, indexAfter;
            element.sortable({
                start: function(event, ui){
                    indexBefore = $(ui.item).index();
                },
                stop: function(event, ui) {
                    indexAfter = $(ui.item).index();
                    scope.change({
                        "pindex": indexBefore,
                        "index": indexAfter
                    });
                    console.log("stop event");
                }
            })
        }
    }
});
app.service("modal", function(){
    //$(document.body).append("    <div id=\"_alert\" tabindex=\"-1\" class=\"modal\">"+
    //            "        <div class=\"modal-dialog\">"+
    //            "        <div class=\"modal-content\">"+
    //            "        <div class=\"modal-header\">"+
    //            "        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>"+
    //            "    <p id=\"modal-msg\" class=\"modal-title\">testing alert bla bla bla bla bla</p>"+
    //            "    </div><!-- /.modal-content -->"+
    //            "    </div><!-- /.modal-dialog -->"+
    //            "    </div><!-- /.modal -->"
    //);

    console.log($("#template-modal").text());

    var template = $("#template-modal");
    if(template.length){
        $(document.body).append(template.text());
    }else{
        console.warn("Шаблон отсутствует внутри script с ид template-modal. Шаблон должен содержать " +
            "id modal-msg для контейнера сообщения и id alert для применения бутстраповских функций" +
            "Я бы мог захардкожить темплейт в js,но не хочу.");
    }


    var modal = $("#_alert"),
        msgBox = $("#modal-msg");

    this.setMsg = function(text){
        msgBox.text(text);
    };
    this.show = function(msg){
        this.setMsg(msg);
        modal.modal("show");
    };
    this.hide = function(){
        modal.modal("hide");
    };
});

app.controller("mapCtrl", function($scope, map, modal){
    //fixtures
    $scope.points = [
        "героев панфиловцев",
        "шоссе энтузиастов",
        "ленинский проспект"
    ];

    $scope.names = [];

    $scope.remove = function(index){
        $scope.points.splice(index, 1);
    };

    $scope.changeModel = function(pindex, index){
        var swap = $scope.points[pindex];
        $scope.points.splice(pindex, 1);
        $scope.points.splice(index, 0, swap);

        if(pindex != index) {
            $scope.names[index] = $scope.names[pindex];
            $scope.names[pindex] = 0;
        }

        $scope.$apply();
    };


    $scope.format = "address";
    $scope.placeholder = {
        "address": "Москва, ул. Героев Панфиловцев, 21",
        "coords": "12.414, 56.42225"
    };

    $scope.addPoint = function() {
        if ($scope.points.indexOf($scope.newPoint) != -1) {
            //alert("нельзя делать дубликаты");
            modal.show("нельзя делать дубликаты");
            return;
        }
        var point;
        if ($scope.format == "coords") {
            if(!($scope.newPoint.split(",").length == 2)){
                //alert("Возможно вы забыли \",\"");
                modal.show("Возможно вы забыли \",\"");
                return;
            }
            if(!($scope.newPoint.split(".").length == 3)){
                //alert("координаты должны быть обязательно указаны плавающим числом через точку");
                modal.show("координаты должны быть обязательно указаны плавающим числом через точку");
                return;
            }
            point = $scope.newPoint.split(",");

            point[0] = parseFloat(point[0]);
            point[1] = parseFloat(point[1]);
            map.geocode(point).then(function(res){
                if(res.geoObjects.getLength() > 0){
                    map.setCenter(point);
                    $scope.points.push(point);
                }
            })
        }

        if ($scope.format == "address") {
            point = $scope.newPoint;
            map.geocode(point).then(function(res){
                if(res.geoObjects.getLength() < 1){
                    //alert(point +" - такая точка на карте не найдена");
                    modal.show(point +" - такая точка на карте не найдена");
                }else{
                    map.setCenter(res.geoObjects.get(0).geometry.getBounds()[0]);
                    $scope.points.push(point);
                }
            })
        }
    };

    map.mapReady().then(
        function(){
            map.init();
            $scope.$watch("points", function(){
                map.route($scope.points, $scope.names);
            }, true);
        }
    );

});