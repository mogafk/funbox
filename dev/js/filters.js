app.filter("coords", function(){
    return function(input){
        if(Object.prototype.toString.call(input) == "[object Array]"){
            return [input[0].toFixed(2), input[1].toFixed(2)];
        }
        return input;
    };
});