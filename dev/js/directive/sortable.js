app.directive("sortable", function(){
    return{
        restrict: "A",
        scope: {
            change: "&"
        },
        link: function(scope, element, attrs){
            var indexBefore, indexAfter;
            element.sortable({
                start: function(event, ui){
                    indexBefore = $(ui.item).index();
                },
                stop: function(event, ui) {
                    indexAfter = $(ui.item).index();
                    scope.change({
                        "pindex": indexBefore,
                        "index": indexAfter
                    });
                    console.log("stop event");
                }
            })
        }
    }
});