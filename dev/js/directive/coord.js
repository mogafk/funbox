app.directive("coord", function(){
    return {
        //restrict: "E",
        //transclude: true,
        //link: function(scope, element, attrs){
        //    console.log("i work");
        //},
        //template: "<span ng-transclude></span>"
        //template: "<li>{{lol}}</li>>"

        require: "ngModel",
        link: function(scope, elm, attrs, ctrl){
            ctrl.$validation.coord = function(modelValue, viewValue){
                if(ctrl.$isEmpty(modelValue))
                    return true;

                if((viewValue.split(".").length == 2) && (viewValue.split(".").length == 3))
                    return true;

                return false;
            }
        }
    }
});