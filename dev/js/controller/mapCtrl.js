app.controller("mapCtrl", function($scope, map, modal){
    //fixtures
    $scope.points = [
        "героев панфиловцев",
        "шоссе энтузиастов",
        "ленинский проспект"
    ];

    $scope.names = [];

    $scope.remove = function(index){
        $scope.points.splice(index, 1);
        $scope.names.splice(index, 1)
    };

    $scope.changeModel = function(pindex, index){
        var swap = $scope.points[pindex];
        $scope.points.splice(pindex, 1);
        $scope.points.splice(index, 0, swap);

        if(pindex != index) {
            $scope.names[index] = $scope.names[pindex];
            $scope.names[pindex] = 0;
        }

        $scope.$apply();
    };


    $scope.format = "address";
    $scope.placeholder = {
        "address": "Красная площадь, москва",
        "coords": "12.414, 56.42225"
    };

    $scope.addPoint = function() {
        if ($scope.points.indexOf($scope.newPoint) != -1) {
            //alert("нельзя делать дубликаты");
            modal.show("нельзя делать дубликаты");
            return;
        }
        var point;
        if ($scope.format == "coords") {
            if(!($scope.newPoint.split(",").length == 2)){
                //alert("Возможно вы забыли \",\"");
                modal.show("Возможно вы забыли \",\"");
                return;
            }
            if(!($scope.newPoint.split(".").length == 3)){
                //alert("координаты должны быть обязательно указаны плавающим числом через точку");
                modal.show("координаты должны быть обязательно указаны плавающим числом через точку");
                return;
            }
            point = $scope.newPoint.split(",");

            point[0] = parseFloat(point[0]);
            point[1] = parseFloat(point[1]);
            map.geocode(point).then(function(res){
                if(res.geoObjects.getLength() > 0){
                    map.setCenter(point);
                    $scope.points.push(point);
                }
            })
        }

        if ($scope.format == "address") {
            point = $scope.newPoint;
            map.geocode(point).then(function(res){
                if(res.geoObjects.getLength() < 1){
                    //alert(point +" - такая точка на карте не найдена");
                    modal.show(point +" - такая точка на карте не найдена");
                }else{
                    map.setCenter(res.geoObjects.get(0).geometry.getBounds()[0]);
                    $scope.points.push(point);
                }
            })
        }
    };

    map.mapReady().then(
        function(){
            map.init();
            $scope.$watch("points", function(){
                map.route($scope.points, $scope.names);
            }, true);
        }
    );

});