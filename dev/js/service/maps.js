app.service("map", function($rootScope,$q){
    var that = this;

    this.mapReady = function(){
        var deffered = $q.defer();
        ymaps.ready(function(){
            deffered.resolve(true);
            return deffered.promise;
        });
        return deffered.promise;
    };
    this.init = function(){
        this.map = new ymaps.Map("map", {
            center: [55.76, 37.64],
            zoom: 10
        });
    };
    this.geocode = function(point){//direct\indirect
        var map = this.map,
            deffered = $q.defer();
        ymaps.geocode(point).then(function(res){
            deffered.resolve(res);
        });
        return deffered.promise;
    };
    this.setCenter = function(coord){
        var map = this.map;
        map.setCenter(coord);
    };
    this.route = function(arrPoints, names) {
        var map = this.map;
        map.geoObjects.removeAll();
        ymaps.route(arrPoints, {"test": "testProp"}).then(function (route) {
            route.getWayPoints().options.set({
                draggable: true
            });
            map.geoObjects.add(route);

            route.getWayPoints().each(function (wayPoint, index) {
                //if it coord
                if(Object.prototype.toString.call(arrPoints[index]) == "[object Array]"){
                    that.geocode(wayPoint.geometry.getBounds()[0]).then(function(res){
                        wayPoint.properties.set("balloonContent", res.geoObjects.get(0).properties.get("name"))
                        names[index] = res.geoObjects.get(0).properties.get("name");
                    });
                }
                wayPoint.events.add("dragend", function (event) {
                    arrPoints[index] = wayPoint.geometry.getBounds()[0];
                    $rootScope.$apply();
                })
            });
            route.getWayPoints().get(0).properties.set("balloonContent", "testContent") //work
        });
    };
});