app.service("modal", function(){
    //$(document.body).append("    <div id=\"_alert\" tabindex=\"-1\" class=\"modal\">"+
    //            "        <div class=\"modal-dialog\">"+
    //            "        <div class=\"modal-content\">"+
    //            "        <div class=\"modal-header\">"+
    //            "        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>"+
    //            "    <p id=\"modal-msg\" class=\"modal-title\">testing alert bla bla bla bla bla</p>"+
    //            "    </div><!-- /.modal-content -->"+
    //            "    </div><!-- /.modal-dialog -->"+
    //            "    </div><!-- /.modal -->"
    //);

    console.log($("#template-modal").text());

    var template = $("#template-modal");
    if(template.length){
        $(document.body).append(template.text());
    }else{
        console.warn("Шаблон отсутствует внутри script с ид template-modal. Шаблон должен содержать " +
            "id modal-msg для контейнера сообщения и id alert для применения бутстраповских функций" +
            "Я бы мог захардкожить темплейт в js,но не хочу.");
    }


    var modal = $("#_alert"),
        msgBox = $("#modal-msg");

    this.setMsg = function(text){
        msgBox.text(text);
    };
    this.show = function(msg){
        this.setMsg(msg);
        modal.modal("show");
    };
    this.hide = function(){
        modal.modal("hide");
    };
});
