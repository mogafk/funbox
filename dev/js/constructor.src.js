(function (global){
    var ym = { modules: global.ymaps.modules };

    (function (modules){
        var project = { DEBUG: false };
        if (typeof modules == 'undefined' && typeof require == 'function') {
            var modules = require('ym');
        }

        modules.define('util.providePackage', ['system.mergeImports'], function (provide, mergeImports) {
            provide(function (srcPackage, packageArgs) {
                var packageProvide = packageArgs[0],
                    packageModules = Array.prototype.slice.call(packageArgs, 1),
                    ns = mergeImports.joinImports(srcPackage.name, {}, srcPackage.deps, packageModules);

                packageProvide(ns);
            });
        });
    })(ym.modules);

    ym.modules.define('CenteredControl', [
        'util.defineClass',
        'util.extend',
        'templateLayoutFactory',
        'collection.Item',
        'data.Manager'
    ], function (provide, defineClass, extend, templateLayoutFactory, CollectionItem, DataManager) {

        var CenteredControlLayout = templateLayoutFactory.createClass([
            '<div class="container-fluid" style="position:absolute; left:{{ options.pos.left }}px; top:{{ options.pos.top }}px;">',
            '{% if options.contentLayout %}',
            '{% include options.contentLayout %}',
            '{% endif %}',
            '</div>'
        ].join(''));

        var CenteredControl = defineClass(function (parameters) {
            CenteredControl.superclass.constructor.call(this, extend({
                float: 'none'
            }, parameters.options));
            this.data = new DataManager(parameters.data);
        }, CollectionItem, {
            onAddToMap: function (map) {
                CenteredControl.superclass.onAddToMap.call(this, map);

                this.getParent().getChildElement(this).then(this._onChildElement, this);
            },
            onRemoveFromMap: function (map) {
                this._clearListeners(map);
                this._layout.setParentElement(null);

                CenteredControl.superclass.onRemoveFromMap.call(this, map);
            },
            _onChildElement: function (element) {
                var layout = this._layout = new CenteredControlLayout({
                    options: this.options,
                    data: this.data,
                    control: this
                });
                layout.setParentElement(element);
                this._setupListeners();
                this._setPosition();
            },
            _setupListeners: function () {
                this.getMap().events
                    .add('sizechange', this._setPosition, this);
            },
            _clearListeners: function (map) {
                map.events
                    .remove('sizechange', this._setPosition, this);
            },
            _setPosition: function () {
                var mapSize = this.getMap().container.getSize(),
                    layoutContentElement = this._layout.getParentElement().firstChild.firstChild;

                this.options.set('pos', {
                    top: Math.round(mapSize[1] / 2 - layoutContentElement.offsetHeight / 2),
                    left: Math.round(mapSize[0] / 2 - layoutContentElement.offsetWidth / 2)
                });
            }
        });

        provide(CenteredControl);
    });

    ym.modules.define('control.CrossControl', [
        'util.defineClass',
        'templateLayoutFactory',
        'collection.Item',
        'data.Manager'
    ], function (provide, defineClass, templateLayoutFactory, CollectionItem, DataManager) {
        /**
         * ÐšÐ»Ð°ÑÑ ÐºÐ¾Ð½Ñ‚Ñ€Ð¾Ð»Ð° "Ñ†ÐµÐ½Ñ‚Ñ€ ÐºÐ°Ñ€Ñ‚Ñ‹".
         * @class
         * @name CrossControl
         */
        var CrossControl = defineClass(function (options) {
            CrossControl.superclass.constructor.call(this, options);

        }, CollectionItem, /** @lends CrossControl.prototype */{
            /**
             * Ð£ÑÑ‚Ð°Ð½Ð°Ð²Ð»Ð¸Ð²Ð°ÐµÑ‚ Ñ€Ð¾Ð´Ð¸Ñ‚ÐµÐ»ÑŒÑÐºÐ¸Ð¹ Ð¾Ð±ÑŠÐµÐºÑ‚.
             * @function
             * @name CrossControl.setParent
             * @param {IControlParent} parent Ð Ð¾Ð´Ð¸Ñ‚ÐµÐ»ÑŒÑÐºÐ¸Ð¹ Ð¾Ð±ÑŠÐµÐºÑ‚.
             * @returns {CrossControl} Ð’Ð¾Ð·Ð²Ñ€Ð°Ñ‰Ð°ÐµÑ‚ ÑÑÑ‹Ð»ÐºÑƒ Ð½Ð° ÑÐµÐ±Ñ.
             */
            setParent: function (parent) {
                this.parent = parent;

                if(parent) {
                    var map = this._map = parent.getMap();
                    this._setPosition(map.container.getSize());
                    this._setupListeners();
                    /**
                     * ÐŸÐµÑ€ÐµÐ´Ð°ÐµÐ¼ Ð² Ð¼Ð°ÐºÐµÑ‚ ÐºÐ¾Ð½Ñ‚Ñ€Ð¾Ð»Ð° Ð´Ð°Ð½Ð½Ñ‹Ðµ Ð¾ ÐµÐ³Ð¾ Ð¾Ð¿Ñ†Ð¸ÑÑ….
                     * @see http://api.yandex.ru/maps/doc/jsapi/2.x/ref/reference/ILayout.xml#constructor-summary
                     */
                    this.layout = new CrossControlLayout({ options: this.options });
                    /**
                     * ÐšÐ¾Ð½Ñ‚Ñ€Ð¾Ð» Ð±ÑƒÐ´ÐµÑ‚ Ð´Ð¾Ð±Ð°Ð²Ð»ÑÑ‚ÑŒÑÑ Ð² pane ÑÐ¾Ð±Ñ‹Ñ‚Ð¸Ð¹, Ñ‡Ñ‚Ð¾Ð±Ñ‹ Ð¸ÑÐºÐ»ÑŽÑ‡Ð¸Ñ‚ÑŒ Ð¸Ð½Ñ‚ÐµÑ€Ð°ÐºÑ‚Ð¸Ð²Ð½Ð¾ÑÑ‚ÑŒ.
                     * @see http://api.yandex.ru/maps/doc/jsapi/2.x/ref/reference/ILayout.xml#setParentElement
                     */
                    this.layout.setParentElement(map.panes.get('events').getElement());
                }
                else {
                    this.layout.setParentElement(null);
                    this._clearListeners();
                }

                return this;
            },
            /**
             * Ð’Ð¾Ð·Ð²Ñ€Ð°Ñ‰Ð°ÐµÑ‚ ÑÑÑ‹Ð»ÐºÑƒ Ð½Ð° Ñ€Ð¾Ð´Ð¸Ñ‚ÐµÐ»ÑŒÑÐºÐ¸Ð¹ Ð¾Ð±ÑŠÐµÐºÑ‚.
             * @see http://api.yandex.ru/maps/doc/jsapi/2.x/ref/reference/IControl.xml#getParent
             * @function
             * @name CrossControl.getParent
             * @returns {IControlParent} Ð¡ÑÑ‹Ð»ÐºÐ° Ð½Ð° Ñ€Ð¾Ð´Ð¸Ñ‚ÐµÐ»ÑŒÑÐºÐ¸Ð¹ Ð¾Ð±ÑŠÐµÐºÑ‚.
             */
            getParent: function () {
                return this.parent;
            },
            /**
             * Ð£ÑÑ‚Ð°Ð½Ð°Ð²Ð»Ð¸Ð²Ð°ÐµÑ‚ ÐºÐ¾Ð½Ñ‚Ñ€Ð¾Ð»Ñƒ Ð¾Ð¿Ñ†Ð¸ÑŽ "position".
             * @function
             * @private
             * @name CrossControl._setPosition
             * @param {Array} size Ð Ð°Ð·Ð¼ÐµÑ€ ÐºÐ¾Ð½Ñ‚ÐµÐ¹Ð½ÐµÑ€Ð° ÐºÐ°Ñ€Ñ‚Ñ‹.
             */
            _setPosition: function (size) {
                // -8, Ñ‚Ð°Ðº ÐºÐ°Ðº ÐºÐ°Ñ€Ñ‚Ð¸Ð½ÐºÐ° 16Ñ…16
                this.options.set('position', {
                    top: size[1] / 2 - 8,
                    right: size[0] / 2 - 8
                });
            },
            _onPositionChange: function (e) {
                this._setPosition(e.get('newSize'));
            },
            _setupListeners: function () {
                this._map.container.events
                    .add('sizechange', this._onPositionChange, this);
            },
            _clearListeners: function () {
                if(this._map) {
                    this._map.container.events
                        .remove('sizechange', this._onPositionChange, this);
                }
            }
        });

        /**
         * ÐœÐ°ÐºÐµÑ‚ ÐºÐ¾Ð½Ñ‚Ñ€Ð¾Ð»Ð°.
         * @see http://api.yandex.ru/maps/doc/jsapi/2.x/ref/reference/templateLayoutFactory.xml
         * @class
         * @name CrossControl.Layout
         */
        var CrossControlLayout = templateLayoutFactory.createClass(
            '<div class="cross-control" style="right:{{ options.position.right }}px; top:{{ options.position.top }}px;"></div>'
        );

        provide(CrossControl);
    });

    ym.modules.define('DemoControl', [
        'CenteredControl',
        'DemoLayout'
    ], function (provide, CenteredControl, DemoLayout) {

        provide(function (data) {
            return new CenteredControl({
                data: data,
                options: {
                    contentLayout: DemoLayout
                }
            });
        });
    });

    ym.modules.define('control.DraggablePlacemark', [
        'util.defineClass',
        'util.extend',
        'util.Dragger',
        'collection.Item',
        'data.Manager',
        'layout.storage',
        'option.presetStorage'
    ], function (provide, defineClass, extend, Dragger, CollectionItem, DataManager, layoutStorage, presetStorage) {

        var ICON_SIZE = [34, 41];
        // var ICON_OFFSET = { top: 48, left: 11 };
        var ICON_OFFSET = { top: 38, left: 11 };
        var DraggablePlacemark = defineClass(function (options) {
            DraggablePlacemark.superclass.constructor.call(this, extend({ color: 'red', cursor: 'grabbing' }, options));

            this.state = new DataManager();
        }, CollectionItem, {
            onAddToMap: function (map) {
                DraggablePlacemark.superclass.onAddToMap.call(this, map);

                this._element = this._createElement();
                this._cursor = null;
                this._dragger = new Dragger({
                    autoStartElement: this._element
                });
                this._dragOffset = [0, 0];
                this.getParent().getChildElement(this).then(this._onChildElement, this);
            },
            onRemoveFromMap: function (map) {
                this._clearListeners(map);
                this.layout.setParentElement(null);
                this._removeElement();

                DraggablePlacemark.superclass.onRemoveFromMap.call(this, map);
            },
            _createElement: function () {
                var elem = document.createElement('div'),
                    size = this.getMap().container.getSize();

                elem.style.position = 'absolute';
                elem.style.width = ICON_SIZE[0] + 'px';
                elem.style.height = ICON_SIZE[1] + 'px';
                elem.style.left = ((size[0] / 2) - ICON_OFFSET.left) + 'px';
                elem.style.top = ((size[1] / 2) - ICON_OFFSET.top) + 'px';

                return elem;
            },
            _removeElement: function () {
                this._element.parentNode.removeChild(this._element);
            },
            _onChildElement: function (parentDomContainer) {
                // parentDomContainer.appendChild(this._element);
                this.getMap().panes.get('events').getElement().appendChild(this._element);

                var preset = presetStorage.get('islands#icon');
                // var Layout = layoutStorage.get(preset.iconLayout.domLayout);
                var Layout = layoutStorage.get(preset.iconLayout.canvasLayout);
                // var Layout = layoutStorage.get('default#image');

                this.layout = new Layout({
                    state: this.state,
                    options: this.options
                });
                this.layout.setParentElement(this._element);
                // Ð—Ð°Ð½ÑƒÐ»ÑÐµÐ¼ ÑÐ¼ÐµÑ‰ÐµÐ½Ð¸Ðµ DOM-ÑÐ»ÐµÐ¼ÐµÐ½Ñ‚Ð° Ð¼ÐµÑ‚ÐºÐ¸
                this._element.firstChild.style.left = 0;
                this._element.firstChild.style.top = 0;
                this._setupListeners();
            },
            _setupListeners: function () {
                this.layout.events
                    .add('mouseenter', this._onMouseEnter, this)
                    .add('mouseleave', this._onMouseLeave, this);
                this.getMap().container.events
                    .add('sizechange', this._onMapContainerSizeChange, this);
                this._dragger.events
                    .add('start', this._onDragStart, this)
                    .add('move', this._onDrag, this)
                    .add('stop', this._onDragStop, this);
            },
            _clearListeners: function (map) {
                this._dragger.events
                    .remove('start', this._onDragStart, this)
                    .remove('move', this._onDrag, this)
                    .remove('stop', this._onDragStop, this);
                map.container.events
                    .remove('sizechange', this._onMapContainerSizeChange, this);
                this.layout.events
                    .remove('mouseenter', this._onMouseEnter, this)
                    .remove('mouseleave', this._onMouseLeave, this);
            },
            _onMouseEnter: function () {
                this._cursor = this.getMap().cursors.push('pointer');
            },
            _onMouseLeave: function () {
                this._cursor.remove();
            },
            _onMapContainerSizeChange: function (e) {
                var newSize = e.get('newSize');
                var oldSize = e.get('oldSize');
                var offset = [
                    (newSize[0] - oldSize[0]) / 2,
                    (newSize[1] - oldSize[1]) / 2
                ];
                var position = this.getPosition();

                this.setPosition([position[0] + offset[0], position[1] + offset[1]]);
            },
            _onDragStart: function (e) {
                var ePos = e.get('position');
                var pPos = this.getPosition();
                this._dragOffset[0] = ePos[0] - pPos[0];
                this._dragOffset[1] = ePos[1] - pPos[1];
                this._cursor.setKey('grabbing');
            },
            _onDrag: function (e) {
                var position = e.get('position');

                this.setPosition(position);
            },
            _onDragStop: function (e) {
                if(this._cursor) {
                    this._cursor.setKey('pointer');
                }
                this._dragOffset = [0, 0];
            },
            setPosition: function (position) {
                var map = this.getMap(),
                    elem = this._element,
                    offset = map.container.getOffset(),
                    dragOffset = this._dragOffset,
                    pos = [position[0] - dragOffset[0], position[1] - dragOffset[1]],
                    coords = map.options.get('projection').fromGlobalPixels(
                        map.converter.pageToGlobal(pos),
                        map.getZoom()
                    );

                this.state.set('markerCenter', coords);

                elem.style.left = (pos[0] - offset[0] - ICON_OFFSET.left) + 'px';
                elem.style.top = (pos[1] - offset[1] - ICON_OFFSET.top) + 'px';
            },
            moveToMapCenter: function () {
                var mapSize = this.getMap().container.getSize();
                this.setPosition(mapSize.map(function (pos) {
                    return pos / 2;
                }));
            },
            getPosition: function () {
                var map = this.getMap(),
                    elem = this._element,
                    offset = map.container.getOffset();

                return [
                    parseFloat(elem.style.left) + offset[0] + ICON_OFFSET.left,
                    parseFloat(elem.style.top) + offset[1] + ICON_OFFSET.top
                ];
            }
        });

        provide(DraggablePlacemark);
    });

    ym.modules.define('GeoObjectEditor', [
        'util.defineClass',
        'Monitor',
        'GeoObject',
        'GeoObjectCollection',
        'GeoObjectEditor.component.DrawingControl',
        'GeoObjectEditor.component.styles'
    ], function (provide, defineClass, Monitor, GeoObject, GeoObjectCollection, DrawingControl, styles) {
        var defaultState = {
            editing: false,
            drawing: false,
            geometryType: 'Point'
        };
        var GeoObjectEditor = defineClass(function (map) {
            GeoObjectEditor.superclass.constructor.apply(this, arguments);

            this._map = map;
            map.geoObjects.add(this);

            this._mapCursor = null;
            this._activeObject = null;
            this._activeObjectEditorMonitor = null;

            this._drawingControl = new DrawingControl();
            map.controls.add(this._drawingControl);

            this.state.set(defaultState);
            this._stateMonitor = new Monitor(this.state);
            this._setupMonitor();

            this._setupListeners();
        }, GeoObjectCollection, {
            _setupListeners: function () {
                this.events
                    .add('editorstatechange', this._onChildEditorStateChange, this)
                    .add('remove', this._onChildRemove, this)
                    .add('mapchange', this._onMapChange, this);
                this._drawingControl.events
                    .add('select', this._onControlSelect, this)
                    .add('deselect', this._onControlDeselect, this);
            },
            _setupMonitor: function () {
                this._stateMonitor
                    .add('drawing', this._onDrawingChange, this)
                    .add('editing', this._onEditingChange, this);
            },
            _onChildEditorStateChange: function (e) {
                var geoObject = e.get('target');

                if(this._activeObject !== geoObject) {
                    this.unsetActiveObject();
                    this.setActiveObject(geoObject);
                    this._onActiveObjectEditingChange(geoObject.editor.state.get('editing'));
                }
            },
            _onChildRemove: function (e) {
                var geoObject = e.get('child');

                if(geoObject === this._activeObject) {
                    this.unsetActiveObject();
                }
            },
            _onDrawingChange: function (isDrawing) {
                if(isDrawing) {
                    this._listenMapClick();
                    this._addMapCursor();
                }
                else {
                    this._removeMapCursor();
                    this._unlistenMapClick();
                }
            },
            _onEditingChange: function (isEditing) {
            },
            _listenMapClick: function () {
                this._map.events.add('click', this._onMapClick, this);
            },
            _unlistenMapClick: function () {
                this._map.events.remove('click', this._onMapClick, this);
            },
            _addMapCursor: function () {
                this._mapCursor = this._map.cursors.push('crosshair');
            },
            _removeMapCursor: function () {
                this._mapCursor.remove();
            },
            startDrawing: function () {
                this.state.set('drawing', true);
            },
            startEditing: function () {
                this.state.set('editing', true);
            },
            stopDrawing: function () {
                this.state.set('drawing', false);
            },
            stopEditing: function () {
                this.state.set('editing', false);
            },
            setActiveObject: function (geoObject) {
                if(geoObject !== this._activeObject) {
                    this._activeObject = geoObject;
                    this._setupActiveObjectEditorMonitor();
                }
            },
            unsetActiveObject: function () {
                var activeObject = this._activeObject;

                if(activeObject) {
                    this._clearActiveObjectEditorMonitor();
                    if(activeObject.editor.state.get('drawing')) {
                        activeObject.editor.stopDrawing();
                    }
                    if(activeObject.editor.state.get('editing')) {
                        activeObject.editor.stopEditing();
                    }
                    if(activeObject.balloon.isOpen()) {
                        activeObject.balloon.close();
                    }
                    activeObject.state.set('editing', false);
                }
            },
            _setupActiveObjectEditorMonitor: function () {
                var activeObject = this._activeObject;

                if(activeObject) {
                    var monitor = this._activeObjectEditorMonitor = new Monitor(activeObject.editor.state);
                    monitor.add('drawing', this._onActiveObjectDrawingChange, this);
                    monitor.add('editing', this._onActiveObjectEditingChange, this);
                }
            },
            _clearActiveObjectEditorMonitor: function () {
                if(this._activeObjectEditorMonitor) {
                    this._activeObjectEditorMonitor.removeAll();
                }

                this._activeObjectEditorMonitor = null;
            },
            _onActiveObjectDrawingChange: function (drawing) {
                var activeObject = this._activeObject;

                if(!drawing) {
                    activeObject.editor.stopEditing();
                    activeObject.balloon.open();
                    this._deselectDrawingControl();
                }
            },
            _onActiveObjectEditingChange: function (editing) {
                var activeObject = this._activeObject;

                activeObject.options.set('draggable', editing);
                activeObject.state.set('editing', editing);
            },
            _onMapClick: function (e) {
                var geoObject = this._createGeoObject(e.get('coords'));

                this.add(geoObject);
                this.setActiveObject(geoObject);
                geoObject.editor.startDrawing();
                this.state.set('drawing', false);
            },
            _onControlSelect: function (e) {
                var target = e.get('target');

                this.state.set({
                    geometryType: target.data.get('geometryType'),
                    drawing: true
                });
                this.unsetActiveObject();
            },
            _onControlDeselect: function (e) {
                var target = e.get('target');
                var geometryType = target.data.get('geometryType');
                var state = this.state;

                if(state.get('geometryType') === geometryType && state.get('drawing')) {
                    state.set('drawing', false);
                }
            },
            _deselectDrawingControl: function () {
                this._drawingControl.each(function (btn) {
                    if(btn.isSelected()) {
                        btn.deselect();
                    }
                });
            },
            _createGeoObject: function (coordinates) {
                var geometryType = this.state.get('geometryType');

                switch(geometryType) {
                    case 'Polygon':
                        coordinates = [[coordinates]];
                        break;
                    case 'LineString':
                        coordinates = [coordinates];
                        break;
                }

                return new GeoObject({
                    geometry: {
                        type: geometryType,
                        coordinates: coordinates
                    }
                }, {
                    preset: 'GeoObjectEditor#' + geometryType
                });
            }
        });

        provide(GeoObjectEditor);
    });

    ym.modules.define('DemoLayout', [
        'templateLayoutFactory',
        'option.presetStorage'
    ], function (provide, templateLayoutFactory, presetStorage) {

        var DemoLayout = templateLayoutFactory.createClass([
            '<div class="well well-white demo" style="width:700px;">',
            '<a class="close" href="#">&times;</a>',
            '<h1>ÐžÐ¿Ñ€ÐµÐ´ÐµÐ»ÐµÐ½Ð¸Ðµ ÐºÐ¾Ð¾Ñ€Ð´Ð¸Ð½Ð°Ñ‚</h1>',
            '<p class="lead">ÐŸÑ€Ð¸Ð»Ð¾Ð¶ÐµÐ½Ð¸Ðµ &laquo;ÐžÐ¿Ñ€ÐµÐ´ÐµÐ»ÐµÐ½Ð¸Ðµ ÐºÐ¾Ð¾Ñ€Ð´Ð¸Ð½Ð°Ñ‚&raquo; Ð¿Ð¾Ð·Ð²Ð¾Ð»ÑÐµÑ‚ Ð¿Ð¾Ð»ÑƒÑ‡Ð°Ñ‚ÑŒ ÐºÐ¾Ð¾Ñ€Ð´Ð¸Ð½Ð°Ñ‚Ñ‹ Ð¼ÐµÑÑ‚, Ð¾Ð±Ð»Ð°ÑÑ‚ÐµÐ¹ Ð° Ñ‚Ð°ÐºÐ¶Ðµ Ñ€Ð°Ð·Ð»Ð¸Ñ‡Ð½Ñ‹Ñ… Ñ‚Ð¸Ð¿Ð¾Ð² Ð³ÐµÐ¾Ð¾Ð±ÑŠÐµÐºÑ‚Ð¾Ð² Ð´Ð»Ñ Ð´Ð°Ð»ÑŒÐ½ÐµÐ¹ÑˆÐµÐ³Ð¾ Ð¸ÑÐ¿Ð¾Ð»ÑŒÐ·Ð¾Ð²Ð°Ð½Ð¸Ñ Ð¸Ñ… Ð² <strong>API&nbsp;Ð¯Ð½Ð´ÐµÐºÑ.ÐšÐ°Ñ€Ñ‚</strong>.</p>',
            '<p class="lead">ÐŸÑ€Ð¸Ð»Ð¾Ð¶ÐµÐ½Ð¸Ðµ Ð¸ÑÐ¿Ð¾Ð»ÑŒÐ·ÑƒÐµÑ‚ Ð¿Ð¾Ñ€ÑÐ´Ð¾Ðº ÐºÐ¾Ð¾Ñ€Ð´Ð¸Ð½Ð°Ñ‚&nbsp;',
            '<strong>',
            '{% if data.coordOrder == "latlong" %}',
            'ÑˆÐ¸Ñ€Ð¾Ñ‚Ð°-Ð´Ð¾Ð»Ð³Ð¾Ñ‚Ð°',
            '{% else %}',
            'Ð´Ð¾Ð»Ð³Ð¾Ñ‚Ð°-ÑˆÐ¸Ñ€Ð¾Ñ‚Ð°',
            '{% endif %}',
            '</strong>',
            '.</p>',
            '<p class="lead">Ð˜Ð½Ñ„Ð¾Ñ€Ð¼Ð°Ñ†Ð¸Ñ Ð¾ ÑÐ¾ÑÑ‚Ð¾ÑÐ½Ð¸Ð¸ ÐºÐ°Ñ€Ñ‚Ñ‹ Ð´Ð¾ÑÑ‚ÑƒÐ¿Ð½Ð° Ð² Ð¿Ñ€Ð°Ð²Ð¾Ð¼ Ð½Ð¸Ð¶Ð½ÐµÐ¼ ÑƒÐ³Ð»Ñƒ ÐºÐ°Ñ€Ñ‚Ñ‹.</p>',
            '<p class="lead"Ð”Ð»Ñ ÑÐ¾Ð·Ð´Ð°Ð½Ð¸Ñ Ð³ÐµÐ¾Ð¾Ð±ÑŠÐµÐºÑ‚Ð¾Ð² Ñ€Ð°Ð·Ð»Ð¸Ñ‡Ð½Ñ‹Ñ… Ð³ÐµÐ¾Ð¼ÐµÑ‚Ñ€Ð¸Ð¹ Ð½ÑƒÐ¶Ð½Ð¾ Ð¸ÑÐ¿Ð¾Ð»ÑŒÐ·Ð¾Ð²Ð°Ñ‚ÑŒ Ñ‚ÑƒÐ»Ð±Ð°Ñ€ Ð² Ð»ÐµÐ²Ð¾Ð¼ Ð²ÐµÑ€Ñ…Ð½ÐµÐ¼ ÑƒÐ³Ð»Ñƒ ÐºÐ°Ñ€Ñ‚Ñ‹.</p>',
            '<a class="btn btn-success btn-large" href="#">ÐŸÑ€Ð¾Ð´Ð¾Ð»Ð¶Ð¸Ñ‚ÑŒ</a>&nbsp;',
            '<a class="btn btn-warning btn-large" href="?coordorder={% if data.coordOrder == "latlong" %}longlat{% else %}latlong{% endif %}">Ð¡Ð¼ÐµÐ½Ð¸Ñ‚ÑŒ Ð¿Ð¾Ñ€ÑÐ´Ð¾Ðº ÐºÐ¾Ð¾Ñ€Ð´Ð¸Ð½Ð°Ñ‚</a>',
            '</div>'
        ].join(''), {
            build: function () {
                DemoLayout.superclass.build.call(this);

                this._setupListeners();
            },
            clear: function () {
                this._clearListeners();

                DemoLayout.superclass.clear.call(this);
            },
            _setupListeners: function () {
                jQuery(this.getElement())
                    .on('click', '.close,.btn-success', jQuery.proxy(this._onClose, this));
            },
            _clearListeners: function () {
                jQuery(this.getElement())
                    .off('click');
            },
            _onClose: function (e) {
                e.preventDefault();

                var control = this.getData().control;

                control.getParent().remove(control);
            }
        });

        presetStorage.add('popup#demo', {
            contentBodyLayout: DemoLayout
        });

        provide(DemoLayout);
    });

    ym.modules.define('MapStateInfo', [
        'util.defineClass',
        'util.extend',
        'util.bind',
        'util.bounds',
        'data.Manager',
        'event.Manager',
        'Monitor',
        'control.MapStateInfoButton',
        'control.MapStateInfoWindow',
        'control.CrossControl',
        'control.DraggablePlacemark'
    ], function (
        provide,
        defineClass,
        extend,
        bind,
        bounds,
        DataManager,
        EventManager,
        Monitor,
        MapStateInfoButton,
        MapStateInfoWindow,
        CrossControl,
        DraggablePlacemark
    ) {
        var MapStateInfo = defineClass(function (map) {
            this._map = map;
            this._mapEvents = map.events.group();

            this.events = new EventManager();

            this.state = new DataManager();

            this._updateTimeout = 30;
            this._intervalId = null;

            this._windowControl = new MapStateInfoWindow();
            this._crossControl = new CrossControl();
            this._markerControl = new DraggablePlacemark();
            this._stateMonitor = new Monitor(this._markerControl.state);
            this._buttonControl = new MapStateInfoButton();
            map.controls.add(this._buttonControl);
            this._buttonControl.events
                .add('select', this._onButtonSelect, this)
                .add('deselect', this._onButtonDeselect, this);
            this._buttonControl.select();
        }, {
            getMap: function () {
                return this._map;
            },
            _setupListeners: function () {
                this._mapEvents
                    .add('boundschange', this._onMapBoundsChange, this)
                    .add('actiontick', this._onMapAction, this)
                    /* Ð’Ð¾ Ð²Ñ€ÐµÐ¼Ñ Ð¿Ð»Ð°Ð²Ð½Ð¾Ð³Ð¾ Ð´Ð²Ð¸Ð¶ÐµÐ½Ð¸Ñ ÐºÐ°Ñ€Ñ‚Ñ‹, Ñƒ Ð±Ñ€Ð°ÑƒÐ·ÐµÑ€Ð¾Ð² Ð¿Ð¾Ð´Ð´ÐµÑ€Ð¶Ð¸Ð²Ð°ÑŽÑ‰Ð¸Ñ… CSS3 Transition,
                     * actiontick Ð½Ðµ ÐºÐ¸Ð´Ð°ÐµÑ‚ÑÑ, Ð¿Ð¾ÑÑ‚Ð¾Ð¼Ñƒ Ð¸ÑÐ¿Ð¾Ð»ÑŒÐ·ÑƒÐµÐ¼ ÑÑ‚Ð¾Ñ‚ Ð¿Ñ€Ð¸ÐµÐ¼ Ñ‡ÐµÑ€ÐµÐ· setInterval.
                     */
                    .add('actionbegin', this._onMapActionBegin, this)
                    .add('actionend', this._onMapActionEnd, this);
                this._map.controls.get('searchControl').events.add('resultshow', this._onSearchControlResult, this);
                this.state.events.add('change', this._onStateChange, this);
            },
            _clearListeners: function () {
                this.state.events.remove('change', this._onStateChange, this);
                this._map.controls.get('searchControl').events.remove('resultshow', this._onSearchControlResult, this);
                this._mapEvents.removeAll();
            },
            _setupMonitor: function () {
                this._stateMonitor
                    .add('markerCenter', this._onMarkerMove, this);
            },
            _clearMonitor: function () {
                this._stateMonitor
                    .removeAll();
            },
            _onSearchControlResult: function () {
                this._markerControl.moveToMapCenter();
            },
            _onButtonSelect: function () {
                this._map.controls
                    .add(this._windowControl, { position: { bottom: 190, right: 230 }})
                    .add(this._crossControl)
                    .add(this._markerControl);

                this._setupListeners();
                this._setupMonitor();
                this.state.set(this._getInitialState());
            },
            _onButtonDeselect: function () {
                this._map.controls
                    .remove(this._windowControl)
                    .remove(this._crossControl)
                    .remove(this._markerControl);

                this._clearListeners();
                this._clearMonitor();
            },
            _getInitialState: function () {
                var map = this.getMap();

                return {
                    mapCenter: map.getCenter(),
                    mapZoom: map.getZoom(),
                    mapBounds: map.getBounds(),
                    markerCenter: map.getCenter()
                };
            },
            _onMarkerMove: function (newCenter) {
                this.state.set('markerCenter', newCenter);
            },
            /**
             * ÐžÐ±Ñ€Ð°Ð±Ð¾Ñ‚Ñ‡Ð¸Ðº Ð½Ð°Ñ‡Ð°Ð»Ð° Ð¿Ð»Ð°Ð²Ð½Ð¾Ð³Ð¾ Ð´Ð²Ð¸Ð¶ÐµÐ½Ð¸Ñ ÐºÐ°Ñ€Ñ‚Ñ‹.
             * @see http://api.yandex.ru/maps/doc/jsapi/2.x/ref/reference/Map.xml#event-actionbegin
             * @function
             * @private
             * @name MapView._onMapActionBegin
             * @param {ymaps.Event} e ÐžÐ±ÑŠÐµÐºÑ‚-ÑÐ¾Ð±Ñ‹Ñ‚Ð¸Ðµ
             */
            _onMapActionBegin: function (e) {
                if(this._intervalId) {
                    return;
                }

                this._intervalId = window.setInterval(
                    bind(this._onMapAction, this),
                    this._updateTimeout
                );
            },
            /**
             * ÐžÐ±Ñ€Ð°Ð±Ð¾Ñ‚Ñ‡Ð¸Ðº Ð¾ÐºÐ¾Ð½Ñ‡Ð°Ð½Ð¸Ñ Ð¿Ð»Ð°Ð²Ð½Ð¾Ð³Ð¾ Ð´Ð²Ð¸Ð¶ÐµÐ½Ð¸Ñ ÐºÐ°Ñ€Ñ‚Ñ‹.
             * @see http://api.yandex.ru/maps/doc/jsapi/2.x/ref/reference/Map.xml#event-actionend
             * @function
             * @private
             * @name MapView._onMapActionEnd
             * @param {ymaps.Event} e ÐžÐ±ÑŠÐµÐºÑ‚-ÑÐ¾Ð±Ñ‹Ñ‚Ð¸Ðµ
             */
            _onMapActionEnd: function (e) {
                window.clearInterval(this._intervalId);
                this._intervalId = null;
            },
            /**
             * ÐžÐ±Ñ€Ð°Ð±Ð¾Ñ‚Ñ‡Ð¸Ðº Ð¸ÑÐ¿Ð¾Ð»Ð½ÐµÐ½Ð¸Ñ Ð½Ð¾Ð²Ð¾Ð³Ð¾ ÑˆÐ°Ð³Ð° Ð¿Ð»Ð°Ð²Ð½Ð¾Ð³Ð¾ Ð´Ð²Ð¸Ð¶ÐµÐ½Ð¸Ñ.
             * @see http://api.yandex.ru/maps/doc/jsapi/2.x/ref/reference/Map.xml#event-actiontick
             * @function
             * @private
             * @name MapView._onMapAction
             * @param {ymaps.Event} e ÐžÐ±ÑŠÐµÐºÑ‚-ÑÐ¾Ð±Ñ‹Ñ‚Ð¸Ðµ
             */
            _onMapAction: function (e) {
                /**
                 * ÐžÐ¿Ñ€ÐµÐ´ÐµÐ»ÑÐµÑ‚ ÑÐ¾ÑÑ‚Ð¾ÑÐ½Ð¸Ðµ ÐºÐ°Ñ€Ñ‚Ñ‹ Ð² Ð¼Ð¾Ð¼ÐµÐ½Ñ‚ ÐµÐµ Ð¿Ð»Ð°Ð²Ð½Ð¾Ð³Ð¾ Ð´Ð²Ð¸Ð¶ÐµÐ½Ð¸Ñ.
                 * @see http://api.yandex.ru/maps/doc/jsapi/2.x/ref/reference/map.action.Manager.xml#getCurrentState
                 */
                var map = this.getMap(),
                    projection =  map.options.get('projection'),
                    state = map.action.getCurrentState(),
                    zoom = state.zoom,
                    /**
                     * ÐŸÑ€ÐµÐ¾Ð±Ñ€Ð°Ð·ÑƒÐµÑ‚ Ð¿Ð¸ÐºÑÐµÐ»ÑŒÐ½Ñ‹Ðµ ÐºÐ¾Ð¾Ñ€Ð´Ð¸Ð½Ð°Ñ‚Ñ‹ Ð½Ð° ÑƒÐºÐ°Ð·Ð°Ð½Ð½Ð¾Ð¼ ÑƒÑ€Ð¾Ð²Ð½Ðµ Ð¼Ð°ÑÑˆÑ‚Ð°Ð±Ð¸Ñ€Ð¾Ð²Ð°Ð½Ð¸Ñ Ð² ÐºÐ¾Ð¾Ñ€Ð´Ð¸Ð½Ð°Ñ‚Ñ‹ Ð¿Ñ€Ð¾ÐµÐºÑ†Ð¸Ð¸ (Ð³ÐµÐ¾ÐºÐ¾Ð¾Ñ€Ð´Ð¸Ð½Ð°Ñ‚Ñ‹).
                     * @see http://api.yandex.ru/maps/doc/jsapi/2.x/ref/reference/IProjection.xml#fromGlobalPixels
                     */
                    center = projection.fromGlobalPixels(
                        state.globalPixelCenter, zoom
                    ),
                    containerSize = map.container.getSize(),
                    pixelBounds = [
                        [
                            state.globalPixelCenter[0] - containerSize[0] / 2,
                            state.globalPixelCenter[1] - containerSize[1] / 2
                        ],
                        [
                            state.globalPixelCenter[0] + containerSize[0] / 2,
                            state.globalPixelCenter[1] + containerSize[1] / 2
                        ]
                    ];

                this.state.set({
                    mapCenter: center,
                    mapBounds: bounds.fromGlobalPixelBounds(pixelBounds, zoom, projection),
                    mapZoom: zoom,
                    markerCenter: this._getMarkerCoordinates()
                });
            },
            /**
             * ÐžÐ±Ñ€Ð°Ð±Ð¾Ñ‚Ñ‡Ð¸Ðº ÑÐ¾Ð±Ñ‹Ñ‚Ð¸Ñ Ð¸Ð·Ð¼ÐµÐ½ÐµÐ½Ð¸Ñ Ð¾Ð±Ð»Ð°ÑÑ‚Ð¸ Ð¿Ñ€Ð¾ÑÐ¼Ð¾Ñ‚Ñ€Ð° ÐºÐ°Ñ€Ñ‚Ñ‹ (Ð² Ñ€ÐµÐ·ÑƒÐ»ÑŒÑ‚Ð°Ñ‚Ðµ Ð¸Ð·Ð¼ÐµÐ½ÐµÐ½Ð¸Ñ Ñ†ÐµÐ½Ñ‚Ñ€Ð° Ð¸Ð»Ð¸ ÑƒÑ€Ð¾Ð²Ð½Ñ Ð¼Ð°ÑÑˆÑ‚Ð°Ð±Ð¸Ñ€Ð¾Ð²Ð°Ð½Ð¸Ñ)
             * @see http://api.yandex.ru/maps/doc/jsapi/2.x/ref/reference/Map.xml#event-boundschange
             * @function
             * @private
             * @name MapView._onMapBoundsChange
             * @param {ymaps.Event} e ÐžÐ±ÑŠÐµÐºÑ‚-ÑÐ¾Ð±Ñ‹Ñ‚Ð¸Ðµ
             */
            _onMapBoundsChange: function (e) {
                this.state.set({
                    mapCenter: e.get('newCenter'),
                    mapZoom: e.get('newZoom'),
                    mapBounds: e.get('newBounds'),
                    markerCenter: this._getMarkerCoordinates()
                });
            },
            _getMarkerCoordinates: function () {
                var map = this.getMap(),
                    zoom = map.getZoom(),
                    position = this._markerControl.getPosition();

                return map.options.get('projection').fromGlobalPixels(
                    map.converter.pageToGlobal(position, zoom),
                    zoom
                );
            },
            _onStateChange: function () {
                this.events.fire('statechange', extend({ target: this }, this.state.getAll()));
                this._windowControl.data.set(this.state.getAll());
            }
        });

        provide(MapStateInfo);
    });

    ym.modules.define('layout.MapStateInfoWindowLayout', [
        'templateLayoutFactory',
        'layout.storage',
        'template.filter.json'
    ], function (provide, templateLayoutFactory, layoutStorage) {
        var MapStateInfoWindowLayout = templateLayoutFactory.createClass([
            '<div class="popover top mapstateinfo-window">',
            '<div class="popover-inner">',
            '<form class="form-horizontal">',
            '<div class="control-group">',
            '<label class="control-label" for="markerCenter">ÐœÐµÑ‚ÐºÐ°</label>',
            '<div class="controls">',
            '<input type="text" id="markerCenter" class="input-mini" value="{{ data.markerCenter|json }}">',
            '</div>',
            '</div>',
            '<div class="control-group">',
            '<label class="control-label" for="mapZoom">ÐœÐ°ÑÑˆÑ‚Ð°Ð±</label>',
            '<div class="controls">',
            '<input type="text" id="mapZoom" class="input-mini" value="{{ data.mapZoom }}">',
            '</div>',
            '</div>',
            '<div class="control-group">',
            '<label class="control-label" for="mapCenter">Ð¦ÐµÐ½Ñ‚Ñ€</label>',
            '<div class="controls">',
            '<input type="text" id="mapCenter" class="input-mini" value="{{ data.mapCenter|json }}">',
            '</div>',
            '</div>',
            '<div class="control-group">',
            '<label class="control-label" for="mapBounds">ÐžÐ±Ð»Ð°ÑÑ‚ÑŒ</label>',
            '<div class="controls">',
            '<input type="text" id="mapBounds" class="input-mini" value="{{ data.mapBounds|json }}">',
            '</div>',
            '</div>',
            '<input type="submit" style="position: absolute; left: -9999px"/>',
            '</form>',
            '</div>',
            '</div>'
        ].join(''), {
            build: function () {
                MapStateInfoWindowLayout.superclass.build.call(this);

                this._setupListeners();
            },
            clear: function () {
                this._clearListeners();

                MapStateInfoWindowLayout.superclass.clear.call(this);
            },
            _setupListeners: function () {
                var element = this.getElement();

                jQuery('form', element).on('submit', jQuery.proxy(this._onStateSubmit, this));
            },
            _clearListeners: function () {
                var element = this.getElement();

                jQuery('form', element).off('submit');
            },
            _parseCoordinates: function (s) {
                return s.match(/[\d.]+/g).map(Number);
            },
            _onStateSubmit: function (e) {
                e.preventDefault();

                var element = this.getElement();
                var newState = {};
                var mapCenter = jQuery('#mapCenter', element);
                var mapZoom = jQuery('#mapZoom', element);
                var mapBounds = jQuery('#mapBounds', element);
                var center = this._parseCoordinates(mapCenter.val());
                var zoom = Number.parseFloat(mapZoom.val());
                var bbox = this._parseCoordinates(mapBounds.val());

                if(center.every(isFinite) && center.length === 2 && mapCenter.val() != mapCenter[0].defaultValue) {
                    newState['mapCenter'] = center;
                }
                if(zoom >= 0 && mapZoom.val() != mapZoom[0].defaultValue) {
                    newState['mapZoom'] = zoom;
                }
                if(bbox.every(isFinite) && bbox.length === 4 && mapBounds.val() != mapBounds[0].defaultValue) {
                    newState['mapBounds'] = [bbox.slice(0, 2), bbox.slice(2, 4)];
                }

                this.getData().control.state.set(newState);
            }
        });

        layoutStorage.add('mapstate#window', MapStateInfoWindowLayout);

        provide(layoutStorage);
        // provide(MapStateInfoWindowLayout);
    });

    ym.modules.define('control.MapStateInfoButton', [
        'control.Button',
        'util.extend'
    ], function (provide, Button, extend) {
        var defaultOptions = {
            position: {
                right: 10,
                bottom: 64
            }
        };
        var defaultData = {
            image: 'i/button-mapstate.png',
            title: 'Ð¡Ð¾ÑÑ‚Ð¾ÑÐ½Ð¸Ðµ ÐºÐ°Ñ€Ñ‚Ñ‹'
        };
        var MapStateInfoButton = function (params) {
            params = params || {};
            var button = new Button({
                data: extend(defaultData, params.data),
                options: extend(defaultOptions, params.options)
            });

            return button;
        };

        provide(MapStateInfoButton);
    });

    ym.modules.define('control.MapStateInfoWindow', [
        'util.defineClass',
        'collection.Item',
        'data.Manager',
        'Monitor',
        'layout.MapStateInfoWindowLayout'
    ], function (provide, defineClass, CollectionItem, DataManager, Monitor, layoutStorage) {
        var MapStateInfoWindow = defineClass(function (options) {
            MapStateInfoWindow.superclass.constructor.call(this, options);

            this.data = new DataManager();
            this.state = new DataManager();
            this._stateMonitor = new Monitor(this.state);
        }, CollectionItem, {
            onAddToMap: function (map) {
                MapStateInfoWindow.superclass.onAddToMap.call(this, map);

                this.state.set({
                    mapCenter: map.getCenter(),
                    mapZoom: map.getZoom(),
                    mapBounds: map.getBounds()
                });
                this._setupStateMonitor();
                this.getParent().getChildElement(this).then(this._onChildElement, this);
            },
            onRemoveFromMap: function (oldMap) {
                this._clearStateMonitor();

                MapStateInfoWindow.superclass.onRemoveFromMap.call(this, oldMap);
            },
            _setupStateMonitor: function () {
                this._stateMonitor
                    .add('mapCenter', this._onMapCenterChange, this)
                    .add('mapZoom', this._onMapZoomChange, this)
                    .add('mapBounds', this._onMapBoundsChange, this)
            },
            _clearStateMonitor: function () {
                this._stateMonitor.removeAll();
            },
            _onMapCenterChange: function (newCenter) {
                this.getMap().setCenter(newCenter);
            },
            _onMapZoomChange: function (newZoom) {
                this.getMap().setZoom(newZoom, {
                    checkZoomRange: true
                });
            },
            _onMapBoundsChange: function (newBounds) {
                this.getMap().setBounds(newBounds, {
                    checkZoomRange: true
                });
            },
            _onChildElement: function (parentDomContainer) {
                var Layout = layoutStorage.get('mapstate#window');

                this.layout = new Layout({ control: this, data: this.data, options: this.options });
                /**
                 * ÐšÐ¾Ð½Ñ‚Ñ€Ð¾Ð» Ð±ÑƒÐ´ÐµÑ‚ Ð´Ð¾Ð±Ð°Ð²Ð»ÑÑ‚ÑŒÑÑ Ð² pane ÑÐ¾Ð±Ñ‹Ñ‚Ð¸Ð¹, Ñ‡Ñ‚Ð¾Ð±Ñ‹ Ð¸ÑÐºÐ»ÑŽÑ‡Ð¸Ñ‚ÑŒ Ð¸Ð½Ñ‚ÐµÑ€Ð°ÐºÑ‚Ð¸Ð²Ð½Ð¾ÑÑ‚ÑŒ.
                 * @see http://api.yandex.ru/maps/doc/jsapi/2.x/ref/reference/ILayout.xml#setParentElement
                 */
                this.layout.setParentElement(parentDomContainer);
            }
        });

        provide(MapStateInfoWindow);
    });

    ym.modules.define('control.RadioGroup', [
        'util.defineClass',
        'util.extend',
        'util.id',
        'Collection',
        'data.Manager',
        'Monitor',
        'vow',
        'event.Mapper',
        'control.RadioGroup.component.EventMappingTable'
    ], function (provide, defineClass, extend, Id, Collection, DataManager, Monitor, vow, EventMapper, EventMappingTable) {
        var defaultOptions = {
            margin: 5
        };
        /**
         * ÐšÐ»Ð°ÑÑ Ñ€Ð°Ð´Ð¸Ð¾Ð³Ñ€ÑƒÐ¿Ð¿Ñ‹ Ð´Ð»Ñ ÐºÐ½Ð¾Ð¿Ð¾Ðº.
         * @class
         * @name RadioGroup
         */
        var RadioGroup = defineClass(function (options) {
            RadioGroup.superclass.constructor.call(this, extend({}, defaultOptions, options));
            this.state = new DataManager();
            this._optionMonitor = new Monitor(this.options);
            this._element = document.createElement('ymaps');
            this._childElements = {};
            this._setupListeners();
            this._setupMonitor();
        }, Collection, /** @lends RadioGroup.prototype */{
            add: function (child) {
                var parent = this.getParent();

                if(parent) {
                    child.options.setParent(parent.options);
                }
                child.events.setParent(new EventMapper(this.events, new EventMappingTable(child)));
                child.setParent(this);

                return RadioGroup.superclass.add.call(this, child);
            },
            remove: function (child) {
                var id = Id.get(child);

                child.setParent(null);
                child.options.setParent(null);
                child.events.setParent(null);
                delete this._childElements[id];

                return RadioGroup.superclass.remove.call(this, child);
            },
            setParent: function (parent) {
                parent.getChildElement(this).then(this._onElement, this);

                return RadioGroup.superclass.setParent.call(this, parent);
            },
            getChildElement: function (child) {
                var id = Id.get(child);
                var el = this._childElements[id] = this._createChildElement();
                this._element.appendChild(el);

                return vow.resolve(el);
            },
            _createChildElement: function () {
                var el = document.createElement('ymaps');

                this._setChildElementMargin(el);

                return el;
            },
            _setChildElementMargin: function (el) {
                var options = this.options,
                    side = options.get('float', 'right'),
                    margin = parseFloat(options.get('margin', 5)) + 'px';

                el.style['margin'] = side === 'left'? '0 ' + margin + ' 0 0': '0 0 0 ' + margin;
            },
            _onElement: function (parentContainer) {
                parentContainer.appendChild(this._element);
            },
            _setupListeners: function () {
                this.events
                    .add('select', this._onChildSelect, this)
                    .add('parentchange', this._onParentChange, this);
            },
            _setupMonitor: function () {
                this._optionMonitor
                    .add(['float', 'margin'], this._updateChildMargin, this);
            },
            _updateChildMargin: function () {
                this.each(function (child) {
                    var el = this._childElements[Id.get(child)];

                    this._setChildElementMargin(el);
                }, this);
            },
            _onParentChange: function (e) {
                var parent = this.getParent();

                this.each(function (child) {
                    child.options.setParent(parent && parent.options || null);
                });
            },
            _onChildSelect: function (e) {
                var target = e.get('target');

                this.each(function (child) {
                    if(target !== child && child.isSelected()) {
                        child.deselect();
                    }
                });
            }
        });

        provide(RadioGroup);
    });

    ym.modules.define('GeoObjectEditor.component.DrawingControl', [
        'util.extend',
        'control.Button',
        'control.RadioGroup'
    ], function (provide, extend, Button, RadioGroup) {
        var defaultOptions = {
            float: 'left'
        };
        var DrawingControl = function (options) {
            var placemarkBtn = new Button({
                data: {
                    geometryType: 'Point',
                    image: 'i/button-placemark.png'
                }
            });
            var polylineBtn = new Button({
                data: {
                    geometryType: 'LineString',
                    image: 'i/button-polyline.png'
                }
            });
            var polygonBtn = new Button({
                data: {
                    geometryType: 'Polygon',
                    image: 'i/button-polygon.png'
                }
            });
            var radioGroup = new RadioGroup(extend(defaultOptions, options));

            radioGroup
                .add(placemarkBtn)
                .add(polylineBtn)
                .add(polygonBtn);

            return radioGroup;
        };

        provide(DrawingControl);
    });

    ym.modules.define('GeoObjectEditor.component.styles', [
        'templateLayoutFactory',
        'util.bind',
        'option.presetStorage',
        'template.filter.geometry.json',
        'template.filter.geometry.base64',
        'template.filter.geometry.bounds'
    ], function (provide, templateLayoutFactory, bind, presetStorage) {

        var BalloonContentLayout = templateLayoutFactory.createClass([
            '<div style="margin:20px 0">',
            '<form>',
            '<div class="control-group">',
            '<label class="control-label" for="coordinatesField">ÐšÐ¾Ð¾Ñ€Ð´Ð¸Ð½Ð°Ñ‚Ñ‹</label>',
            '<div class="controls">',
            '{% include options.coordinatesFieldLayout %}',
            '</div>',
            '</div>',
            '{% if options.boundingBoxFieldLayout %}',
            '<div class="control-group">',
            '<label class="control-label" for="boundingBoxField">ÐžÐ±Ð»Ð°ÑÑ‚ÑŒ</label>',
            '<div class="controls">',
            '{% include options.boundingBoxFieldLayout %}',
            '</div>',
            '</div>',
            '{% endif %}',
            '{% if options.encodedCoordinatesFieldLayout %}',
            '<div class="control-group">',
            '<label class="control-label" for="encodedCoordinatesField">Base64 ÐºÐ¾Ð¾Ñ€Ð´Ð¸Ð½Ð°Ñ‚Ñ‹</label>',
            '<div class="controls">',
            '{% include options.encodedCoordinatesFieldLayout %}',
            '</div>',
            '</div>',
            '{% endif %}',
            '<div class="form-actions">',
            '{% if state.editing %}',
            '<button class="btn btn-warning">Ð—Ð°Ð²ÐµÑ€ÑˆÐ¸Ñ‚ÑŒ</button>&nbsp;',
            '{% else %}',
            '<button type="submit" class="btn btn-success">Ð ÐµÐ´Ð°ÐºÑ‚Ð¸Ñ€Ð¾Ð²Ð°Ñ‚ÑŒ</button>&nbsp;',
            '{% endif %}',
            '<button type="reset" class="btn btn-danger">Ð£Ð´Ð°Ð»Ð¸Ñ‚ÑŒ</button>',
            '</div>',
            '</form>',
            '</div>'
        ].join(''), {
            build: function () {
                BalloonContentLayout.superclass.build.apply(this, arguments);

                this._element = jQuery(this.getParentElement());
                this._geoObject = this.getData().geoObject;
                this._setupListeners();
            },
            clear: function () {
                this._clearListeners();

                BalloonContentLayout.superclass.clear.apply(this, arguments);
            },
            _setupListeners: function () {
                this._element
                    .on('submit', bind(this._onStartEditing, this))
                    .on('reset', bind(this._onRemove, this))
                    .on('click', '.btn-warning', bind(this._onStopEditing, this));
            },
            _clearListeners: function () {
                this._element.off();
            },
            _onStartEditing: function (e) {
                e.preventDefault();

                this._geoObject.editor.startEditing();
                this.events.fire('userclose');
            },
            _onRemove: function (e) {
                e.preventDefault();

                this._geoObject.getParent().remove(this._geoObject);
            },
            _onStopEditing: function (e) {
                e.preventDefault();

                this._geoObject.editor.stopEditing();
            }
        });
        var PointCoordinatesFieldLayout = templateLayoutFactory.createClass('<input id="coordinatesField" class="span4" value="{{ geometry|geometry#json }}"></input>');
        var LineStringCoordinatesFieldLayout = templateLayoutFactory.createClass('<textarea rows="2" id="coordinatesField">{{ geometry|geometry#json }}</textarea>');
        var PolygonCoordinatesFieldLayout = templateLayoutFactory.createClass('<textarea rows="2" id="coordinatesField">{{ geometry|geometry#json }}</textarea>');
        var EncodedCoordinatesFieldLayout = templateLayoutFactory.createClass('<textarea rows="2" id="encodedCoordinatesField">{{ geometry|geometry#base64 }}</textarea>');
        var BoundingBoxFieldLayout = templateLayoutFactory.createClass('<input id="boundingBoxField" class="span4" value="{{ geometry|geometry#bounds }}"></input>');

        presetStorage.add('GeoObjectEditor#Point', {
            balloonContentLayout: BalloonContentLayout,
            balloonCoordinatesFieldLayout: PointCoordinatesFieldLayout
        });
        presetStorage.add('GeoObjectEditor#LineString', {
            balloonContentLayout: BalloonContentLayout,
            balloonCoordinatesFieldLayout: LineStringCoordinatesFieldLayout,
            balloonEncodedCoordinatesFieldLayout: EncodedCoordinatesFieldLayout,
            balloonBoundingBoxFieldLayout: BoundingBoxFieldLayout
        });
        presetStorage.add('GeoObjectEditor#Polygon', {
            balloonContentLayout: BalloonContentLayout,
            balloonCoordinatesFieldLayout: PolygonCoordinatesFieldLayout,
            balloonBoundingBoxFieldLayout: BoundingBoxFieldLayout
        });

        provide(presetStorage);
    });

    ym.modules.define('template.filter.json', [
        'template.filtersStorage'
    ], function (provide, filtersStorage) {

        var toJSON = function (data, value, param) {
            return JSON.stringify(value);
        };

        filtersStorage.add('json', toJSON);

        provide(filtersStorage);
    });

    ym.modules.define('control.RadioGroup.component.EventMappingTable', [
        'util.defineClass',
        'Event'
    ], function (provide, defineClass, Event) {
        var EventMappingTable = function (context) {
            this._context = context;
            this['*'] = this._defaultMappingFunction;

            this['parentchange'] = false;
            this['mapchange'] = false;
            this['optionschange'] = false;
        };

        defineClass(EventMappingTable, {
            _defaultMappingFunction: function (event) {
                return new Event({
                    currentTarget: this._context
                }, event);
            }
        });

        provide(EventMappingTable);
    });

    ym.modules.define('template.filter.geometry.base64', [
        'template.filtersStorage'
    ], function (provide, filtersStorage) {

        var geometryToBase64 = function (data, value, param) {
            var geometry = value.getObject();

            return geometry.constructor.toEncodedCoordinates(geometry);
        };

        filtersStorage.add('geometry#base64', geometryToBase64);

        provide(filtersStorage);
    });

    ym.modules.define('template.filter.geometry.bounds', [
        'template.filtersStorage'
    ], function (provide, filtersStorage) {

        var geometryToJSON = function (data, value, param) {
            var geometry = value.getObject();

            return JSON.stringify(geometry.getBounds());
        };

        filtersStorage.add('geometry#bounds', geometryToJSON);

        provide(filtersStorage);
    });

    ym.modules.define('template.filter.geometry.json', [
        'template.filtersStorage'
    ], function (provide, filtersStorage) {

        var geometryToJSON = function (data, value, param) {
            var geometry = value.getObject();

            return JSON.stringify(geometry.getCoordinates());
        };

        filtersStorage.add('geometry#json', geometryToJSON);

        provide(filtersStorage);
    });

})(this);